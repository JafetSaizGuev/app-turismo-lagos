import 'package:appproyect/Constants.dart';
import 'package:flutter/material.dart';
import '../Interfaz/Interfaz.dart';

class Detalles extends StatefulWidget {
  Detalles({@required this.detalles});
  final Widget detalles;
  @override
  _DetallesState createState() => _DetallesState(Mostrar: detalles);
}

class _DetallesState extends State<Detalles> {
  _DetallesState({@required this.Mostrar});
  final Widget Mostrar;
  double _screenHeight;
  double _bottomSize = 160;
  double _defaultBottomSize = 160;

  @override
  Widget build(BuildContext context) {
    _screenHeight = MediaQuery.of(context).size.height;
    return Positioned(
      bottom: 0,
      child: Container(
        child: GestureDetector(
          onVerticalDragUpdate: (val) {
            setState(() => _bottomSize = _screenHeight - val.globalPosition.dy);
          },
          child: AnimatedContainer(
            duration: Duration(milliseconds: 100),
            width: MediaQuery.of(context).size.width,
            height: _bottomSize,
            constraints: BoxConstraints(
              minHeight: _defaultBottomSize,
              maxHeight: _screenHeight * 0.40,
            ),
            decoration: BoxDecoration(
              color: Colors.black87,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25),
                topRight: Radius.circular(25),
              ),
            ),
            child: Column(
              children: <Widget>[
                Container(
                  width: 50,
                  height: 5,
                  margin: EdgeInsets.symmetric(vertical: 10),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.blueGrey[200],
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        VistaDetalles(ElDetalle: Mostrar,),
                      ],
                    ),
                  ),
                )
                //...details,
              ],
            ),
          ),
        ),
      ),
    );
  }
}


class VistaDetalles extends StatelessWidget {
  VistaDetalles({@required this.ElDetalle});
  Widget ElDetalle;
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.topEnd,
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(margin: EdgeInsets.all(20),),
            Container(child: ElDetalle),
          ],
        ),
      ],
    );
  }
}


