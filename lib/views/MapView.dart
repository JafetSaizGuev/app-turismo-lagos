import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class VistaMapa extends StatelessWidget {
  VistaMapa({this.Refugio,this.Luz,this.Puente,this.Meson,this.Mariano,this.Chava,this.Carol,this.Rinconada,
  this.Terrascali,this.Saroman,this.Vina,this.Rincon,this.Francos,this.CalleJuela,this.Italian,this.Bolonia,
  this.Ochavada,this.Lumaro,this.Wini,this.Remedio,this.Salads,this.Black,this.Galerias,this.CGrande,
  this.Victoria,this.Troje,this.Paris,this.Inn,this.Colonial,this.Calvario,this.Moscati,this.Precidencia,
  this.Cultura,this.Santuario,this.Asuncion,this.Merced,this.Zul,this.Tete,this.Teatro,this.Sorato,this.Rivera,
  this.Plaza,this.Moya,this.Mancha,this.Dilan,this.Bre,this.Bife,this.Anden,
  BitmapDescriptor iconH,BitmapDescriptor iconR,BitmapDescriptor iconI,BitmapDescriptor iconB,BitmapDescriptor iconP})
  {
    _iconMarkerR = iconR;
    _iconMarkerB = iconB;
    _iconMarkerH = iconH;
    _iconMarkerI = iconI;
    _iconMarkerP = iconP;
  }

  Function Refugio;     Function Colonial;    Function Sorato;
  Function Luz;         Function Inn;         Function Salads;
  Function Puente;      Function Paris;       Function Remedio;
  Function Merced;      Function Troje;       Function Anden;
  Function Asuncion;    Function Victoria;    Function Wini;
  Function Teatro;      Function CGrande;     Function Lumaro;
  Function Santuario;   Function Galerias;    Function Mancha;
  Function Plaza;       Function Black;       Function Ochavada;
  Function Cultura;     Function Zul;         Function Bolonia;
  Function Precidencia; Function Tete;        Function Bre;
  Function Moscati;                           Function Italian;
  Function Calvario;                          Function CalleJuela;
  Function Moya;                              Function Dilan;
                                              Function Francos;
                                              Function Rincon;
                                              Function Vina;
                                              Function Saroman;
                                              Function Terrascali;
                                              Function Bife;
                                              Function Rinconada;
                                              Function Rivera;
                                              Function Carol;
                                              Function Chava;
                                              Function Mariano;
                                              Function Meson;

  GoogleMapController _mapController;
  BitmapDescriptor _iconMarkerH,
      _iconMarkerR,
      _iconMarkerI,
      _iconMarkerM,
      _iconMarkerT,
      _iconMarkerB,
      _iconMarkerP;

  Set<Marker> _crearMarcadores() {
    var marcador = Set<Marker>();
    /*-------------------------Historicos-----------------------------------*/
    marcador.add(
      Marker(
          markerId: MarkerId("0"),
          position: LatLng(21.3449994, -101.9375068),
          onTap: Refugio,
          infoWindow:
              InfoWindow(title: "El Refugio", snippet: "Lugar Historico"),
          icon: _iconMarkerI),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("1"),
          position: LatLng(21.359994, -101.926860),
          onTap: Luz,
          infoWindow: InfoWindow(
              title: "Parroquía de Nuestra Señora de la Luz",
              snippet: "Lugar Historico"),
          icon: _iconMarkerI),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("2"),
          position: LatLng(21.3594619, -101.9263641),
          onTap: Puente,
          infoWindow:
              InfoWindow(title: "Puente de Lagos", snippet: "Lugar Historico"),
          icon: _iconMarkerB),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("4"),
          position: LatLng(21.354301, -101.932010),
          onTap: Merced,
          infoWindow: InfoWindow(
              title: "Templo de Nuestra Señora de la Merced",
              snippet: "Lugar Historico"),
          icon: _iconMarkerI),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("5"),
          position: LatLng(21.355540, -101.930754),
          onTap: Asuncion,
          infoWindow: InfoWindow(
              title: "Parroquia de Nuestra Señora de la Asunción",
              snippet: "Lugar Historico"),
          icon: _iconMarkerI),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("7"),
          position: LatLng(21.356431, -101.931119),
          onTap: Teatro,
          infoWindow: InfoWindow(
              title: "Teatro Jose Rosas Moreno", snippet: "Lugar Historico"),
          icon: _iconMarkerP),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("8"),
          position: LatLng(21.356153, -101.931908),
          onTap: Santuario,
          infoWindow: InfoWindow(
              title: "Santuario de Nuestra Señora Guadalupe",
              snippet: "Lugar Historico"),
          icon: _iconMarkerI),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("14"),
          position: LatLng(21.354631, -101.929468),
          onTap: Plaza,
          infoWindow:
          InfoWindow(title: "Plaza Capuchinas", snippet: "Lugar Historico"),
          icon: _iconMarkerP),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("16"),
          position: LatLng(21.354283, -101.929642),
          onTap: Cultura,
          infoWindow: InfoWindow(
              title: "Casa de la Cultura ", snippet: "Lugar Historico"),
          icon: _iconMarkerP),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("27"),
          position: LatLng(21.354447, -101.930830),
          onTap: Precidencia,
          infoWindow: InfoWindow(
              title: "Presidencia Municipal de Lagos de Moreno",
              snippet: "Presidencia Municipal"),
          icon: _iconMarkerP),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("35"),
          position: LatLng(21.359251, -101.937981),
          onTap: Moscati,
          infoWindow: InfoWindow(
              title: "Templo San Jose Moscati", snippet: "Lugar Historico"),
          icon: _iconMarkerI),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("33"),
          position: LatLng(21.361355, -101.934692),
          onTap: Calvario,
          infoWindow:
          InfoWindow(title: "El Calvario", snippet: "Lugar Historico"),
          icon: _iconMarkerI),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("54"),
          position: LatLng(21.365532, -101.918261),
          onTap: Moya,
          infoWindow: InfoWindow(
              title: "Parroquia Inmaculada Concepción de Moya",
              snippet: "Lugar Historico"),
          icon: _iconMarkerI),
    );
    /*---------------------------Hoteles--------------------------------------*/
    marcador.add(
      Marker(
          markerId: MarkerId("10"),
          position: LatLng(21.355441, -101.929977),
          onTap: Colonial,
          infoWindow: InfoWindow(
              title: "Hotel Colonial", snippet: "Lugar de Hospedaje"),
          icon: _iconMarkerH),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("29"),
          position: LatLng(21.355899, -101.931389),
          onTap: Inn,
          infoWindow: InfoWindow(
              title: "Hotel Lagos Inn", snippet: "Lugar de Hospedaje"),
          icon: _iconMarkerH),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("38"),
          position: LatLng(21.354466, -101.930570),
          onTap: Paris,
          infoWindow:
          InfoWindow(title: "Hotel Paris", snippet: "Lugar de Hospedaje"),
          icon: _iconMarkerH),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("39"),
          position: LatLng(21.354993, -101.930068),
          onTap: Troje,
          infoWindow: InfoWindow(
              title: "Hotel la Troje", snippet: "Lugar de Hospedaje"),
          icon: _iconMarkerH),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("40"),
          position: LatLng(21.357737, -101.927767),
          onTap: Victoria,
          infoWindow: InfoWindow(
              title: "Hotel Victoria Lagos", snippet: "Lugar de Hospedaje"),
          icon: _iconMarkerH),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("41"),
          position: LatLng(21.334988, -101.961772),
          onTap: CGrande,
          infoWindow: InfoWindow(
              title: "Hotel Casa Grande", snippet: "Lugar de Hospedaje"),
          icon: _iconMarkerH),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("42"),
          position: LatLng(21.340309, -101.947767),
          onTap: Galerias,
          infoWindow: InfoWindow(
              title: "Hotel Galerias ", snippet: "Lugar de Hospedaje"),
          icon: _iconMarkerH),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("43"),
          position: LatLng(21.352804, -101.941009),
          onTap: Black,
          infoWindow:
          InfoWindow(title: "Motel Black ", snippet: "Lugar de Hospedaje"),
          icon: _iconMarkerH),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("44"),
          position: LatLng(21.353958, -101.931795),
          onTap: Zul,
          infoWindow: InfoWindow(
              title: "Hotel Casa Zul ", snippet: "Lugar de Hospedaje"),
          icon: _iconMarkerH),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("9"),
          position: LatLng(21.356048, -101.930282),
          onTap: Tete,
          infoWindow:
          InfoWindow(title: "La Casona de Teté", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    /*-------------------------Restaurantes-----------------------------------*/
    marcador.add(
      Marker(
          markerId: MarkerId("11"),
          position: LatLng(21.355577, -101.929830),
          onTap: Sorato,
          infoWindow:
          InfoWindow(title: "Sorato Sushi Bar", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("12"),
          position: LatLng(21.355682, -101.929570),
          onTap: Salads,
          infoWindow: InfoWindow(title: "CHEFF SALADS", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("18"),
          position: LatLng(21.353962, -101.930448),
          onTap: Remedio,
          infoWindow: InfoWindow(
              title: "Restaurante Santo Remedio", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("17"),
          position: LatLng(21.353729, -101.930335),
          onTap: Anden,
          infoWindow:
          InfoWindow(title: "ANDEN CINCO 35", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("20"),
          position: LatLng(21.353867, -101.930824),
          onTap: Wini,
          infoWindow: InfoWindow(
              title: "Wini’s Burger´s & Dogo’s", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("19"),
          position: LatLng(21.353237, -101.930013),
          onTap: Lumaro,
          infoWindow: InfoWindow(title: "Lumaro Sushi", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("21"),
          position: LatLng(21.353398, -101.929909),
          onTap: Mancha,
          infoWindow: InfoWindow(title: "La Mancha", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("22"),
          position: LatLng(21.355502, -101.930186),
          onTap: Ochavada,
          infoWindow: InfoWindow(
              title: "La Ochavada de Bistro", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("24"),
          position: LatLng(21.355001, -101.931389),
          onTap: Bolonia,
          infoWindow: InfoWindow(title: "Bolonia", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("23"),
          position: LatLng(21.354994, -101.930971),
          onTap: Bre,
          infoWindow: InfoWindow(title: "Bre Café", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );

    marcador.add(
      Marker(
          markerId: MarkerId("25"),
          position: LatLng(21.354852, -101.930926),
          onTap: Italian,
          infoWindow: InfoWindow(
              title: "The Italian Coffee Company", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("26"),
          position: LatLng(21.354493, -101.931005),
          onTap: CalleJuela,
          infoWindow:
          InfoWindow(title: "La callejuela", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("45"),
          position: LatLng(21.352666, -101.941173),
          onTap: Dilan,
          infoWindow: InfoWindow(title: "Tacos Dilan", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("46"),
          position: LatLng(21.353794, -101.931848),
          onTap: Francos,
          infoWindow:
          InfoWindow(title: "Franco’s Pizza", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("47"),
          position: LatLng(21.353803, -101.931999),
          onTap: Rincon,
          infoWindow: InfoWindow(
              title: "El Rincón de la Merced", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("48"),
          position: LatLng(21.354853, -101.932167),
          onTap: Vina,
          infoWindow: InfoWindow(
              title: "La Viña Restaurant & Bar", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("49"),
          position: LatLng(21.357325, -101.930605),
          onTap: Saroman,
          infoWindow:
          InfoWindow(title: "Casona Sanromán", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("50"),
          position: LatLng(21.349541, -101.943240),
          onTap: Terrascali,
          infoWindow: InfoWindow(
              title: "Terrescali Restaurant", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("28"),
          position: LatLng(21.354236, -101.931410),
          onTap: Bife,
          infoWindow:
          InfoWindow(title: "La Finca del Bife", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("30"),
          position: LatLng(21.354680, -101.931417),
          onTap: Rinconada,
          infoWindow: InfoWindow(title: "La Rinconada", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("31"),
          position: LatLng(21.356142, -101.931599),
          onTap: Rivera,
          infoWindow:
          InfoWindow(title: "Restaurant Rivera", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("32"),
          position: LatLng(21.356591, -101.931908),
          onTap: Carol,
          infoWindow:
          InfoWindow(title: "Carol’s Chiken", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("34"),
          position: LatLng(21.359373, -101.936870),
          onTap: Chava,
          infoWindow: InfoWindow(title: "Chava Tacos", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("36"),
          position: LatLng(21.353499, -101.931981),
          onTap: Mariano,
          infoWindow:
          InfoWindow(title: "Café Mariano Azuela", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );
    marcador.add(
      Marker(
          markerId: MarkerId("37"),
          position: LatLng(21.353103, -101.931463),
          onTap: Meson,
          infoWindow: InfoWindow(
              title: "El Mesón Restaurante-Bar", snippet: "Restaurante"),
          icon: _iconMarkerR),
    );

    /*marcador.add(
      Marker(
          markerId: MarkerId("3"),
          position: LatLng(21.3551707, -101.9426474),
          infoWindow: InfoWindow(
              title: "Parroquía de San Francisco Javier",
              snippet: "Lugar Historico"),
          icon: _iconMarkerI),
    );*/
    /*marcador.add(
      Marker(
          markerId: MarkerId("6"),
          position: LatLng(21.356146, -101.931112),
          infoWindow:
              InfoWindow(title: "Museo de Arte Sacro", snippet: "Museo"),
          icon: _iconMarkerM),
    );*/
    /*marcador.add(
      Marker(
          markerId: MarkerId("13"),
          position: LatLng(21.354549, -101.929283),
          infoWindow:
              InfoWindow(title: "Museo Agustín Rivera", snippet: "Museo"),
          icon: _iconMarkerM),
    );*/
    /*marcador.add(
      Marker(
          markerId: MarkerId("15"),
          position: LatLng(21.354372, -101.929424),
          infoWindow: InfoWindow(
              title: "Parroquia San José", snippet: "Lugar Historico"),
          icon: _iconMarkerI),
    );*/
    /*marcador.add(
      Marker(
          markerId: MarkerId("51"),
          position: LatLng(21.358450, -101.931666),
          infoWindow: InfoWindow(
              title: "Templo la Purisima ", snippet: "Lugar Historico"),
          icon: _iconMarkerI),
    );*/
    /*marcador.add(
      Marker(
          markerId: MarkerId("52"),
          position: LatLng(21.351053, -101.933416),
          infoWindow: InfoWindow(
              title: "Templo de San Felipe de Jesús",
              snippet: "Lugar Historico"),
          icon: _iconMarkerI),
    );*/
    /*marcador.add(
      Marker(
          markerId: MarkerId("53"),
          position: LatLng(21.346555, -101.946086),
          infoWindow: InfoWindow(
              title: "Templo Sagrado Corazón de Jesus",
              snippet: "Lugar Historico"),
          icon: _iconMarkerI),
    );*/
    /*marcador.add(
      Marker(
          markerId: MarkerId("55"),
          position: LatLng(21.349595, -101.943224),
          infoWindow:
              InfoWindow(title: "Museo Casa Carlos Terrés", snippet: "Museo"),
          icon: _iconMarkerM),
    );*/
    /*marcador.add(
      Marker(
          markerId: MarkerId("56"),
          position: LatLng(21.356482, -101.930917),
          infoWindow:
              InfoWindow(title: "Museo Paula Briones", snippet: "Museo"),
          icon: _iconMarkerM),
    );*/
    /*marcador.add(
      Marker(
          markerId: MarkerId("57"),
          position: LatLng(21.355090, -101.931241),
          infoWindow: InfoWindow(
              title: "Tranvia Lagos de Moreno", snippet: "Turistico"),
          icon: _iconMarkerT),
    );*/

    return marcador;
  }

  void _mapCreated(GoogleMapController controller) {
    _mapController = controller;
    _mapController.setMapStyle(
        '[ { "elementType": "geometry", "stylers": [ { "color": "#212121" } ]},{ "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] }, { "elementType": "labels.text.fill", "stylers": [ { "color": "#757575" } ] }, { "elementType": "labels.text.stroke", "stylers": [ { "color": "#212121" } ] }, { "featureType": "administrative", "elementType": "geometry", "stylers": [ { "color": "#757575" }, { "visibility": "off" } ] }, { "featureType": "administrative.country", "elementType": "labels.text.fill", "stylers": [ { "color": "#9e9e9e" } ] }, { "featureType": "administrative.land_parcel", "stylers": [ { "visibility": "off" } ] }, { "featureType": "administrative.locality", "elementType": "labels.text.fill", "stylers": [ { "color": "#bdbdbd" } ] }, { "featureType": "poi", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi", "elementType": "labels.text.fill", "stylers": [ { "color": "#757575" } ] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [ { "color": "#181818" } ] }, { "featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [ { "color": "#616161" } ] }, { "featureType": "poi.park", "elementType": "labels.text.stroke", "stylers": [ { "color": "#1b1b1b" } ] }, { "featureType": "road", "elementType": "geometry.fill", "stylers": [ { "color": "#2c2c2c" } ] }, { "featureType": "road", "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] }, { "featureType": "road", "elementType": "labels.text.fill", "stylers": [ { "color": "#8a8a8a" } ] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [ { "color": "#373737" } ] }, { "featureType": "road.highway", "elementType": "geometry", "stylers": [ { "color": "#3c3c3c" } ] }, { "featureType": "road.highway.controlled_access", "elementType": "geometry", "stylers": [ { "color": "#4e4e4e" } ] }, { "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [ { "color": "#616161" } ] }, { "featureType": "transit", "stylers": [ { "visibility": "off" } ] }, { "featureType": "transit", "elementType": "labels.text.fill", "stylers": [ { "color": "#757575" } ] }, { "featureType": "water", "elementType": "geometry", "stylers": [ { "color": "#000000" } ] }, { "featureType": "water", "elementType": "labels.text.fill", "stylers": [ { "color": "#3d3d3d" } ] } ]');
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
      child: Stack(
        children: <Widget>[
          GoogleMap(
            onMapCreated: _mapCreated,
            mapType: MapType.normal,
            myLocationEnabled: true,
            zoomControlsEnabled: true,
            myLocationButtonEnabled: true,
            markers: _crearMarcadores(),
            initialCameraPosition: CameraPosition(
              target: LatLng(21.355280, -101.930608),
              zoom: 18.0,
            ),
          ),
        ],
      ),
    );
  }
}
