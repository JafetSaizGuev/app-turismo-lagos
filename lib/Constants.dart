import 'package:flutter/material.dart';

const kTextLugar = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

const kTextUbicacion = TextStyle(fontSize: 20);

const kTextResenia = TextStyle(fontSize: 15);

const kDivider = Divider(color: Colors.black,thickness: 2,indent: 70,endIndent: 70,);

const ktoast = Color(0x660000FF);