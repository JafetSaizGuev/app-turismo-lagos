import 'package:appproyect/Constants.dart';
import 'package:appproyect/Interfaz/Lugar.dart';
import 'package:flutter/material.dart';

class Restaurante extends Lugares{
  @override
  Widget Mostrar({@required String lugar,@required String descripcion,@required String direccion, int noHabitaciones = null, double noEstrellas = null}) {
    // TODO: implement Mostrar
    return Column(
      children: <Widget>[
        Container(child: Text(lugar,style: kTextLugar,),padding: EdgeInsets.all(5),),
        Container(child: Text(direccion,style: kTextUbicacion,),padding: EdgeInsets.all(5),),
        Row(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Container(
                padding: EdgeInsets.only(top:10,bottom: 10),
                margin: EdgeInsets.only(left: 15,right: 10),
                child: Text(descripcion,textAlign: TextAlign.center,style: kTextResenia),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                padding: EdgeInsets.only(top:10,bottom: 10),
                margin: EdgeInsets.only(left: 15,right: 10),
                child: Column(
                  children: <Widget>[
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}