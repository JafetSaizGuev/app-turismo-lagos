import 'package:flutter/cupertino.dart';

import 'Lugar.dart';
import 'Hoteles.dart';
import 'Restaurantes.dart';
import 'LugarHistorico.dart';

class Fabrica{
  Lugares GetLugares({TipoLugar tipo}){
    switch(tipo){
      case TipoLugar.Hotel:
        return new Hotel();
        break;
      case TipoLugar.Restaurante:
        return new Restaurante();
        break;
      case TipoLugar.LugarHistorico:
        return new LugarImportante();
        break;
    }
  }
}

enum TipoLugar{
  Hotel,
  LugarHistorico,
  Restaurante,
}