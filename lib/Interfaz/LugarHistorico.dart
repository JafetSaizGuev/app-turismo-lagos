import 'package:appproyect/Constants.dart';
import 'package:appproyect/Interfaz/Lugar.dart';
import 'package:flutter/material.dart';

class LugarImportante extends Lugares{
  @override
  Widget Mostrar({@required String lugar = null,@required String descripcion = null,@required String direccion = null, int noHabitaciones = null, double noEstrellas = null}) {
    return Column(
      children: <Widget>[
        Container(child: Text(lugar,style: kTextLugar,),padding: EdgeInsets.all(5),),
        Container(
          child: Text(direccion,style: kTextUbicacion,),padding: EdgeInsets.all(5),
        ),
        Container(
          padding: EdgeInsets.only(top:10,bottom: 10),
          margin: EdgeInsets.only(left: 15,right: 10),
          child: Text(descripcion,textAlign: TextAlign.center,style: kTextResenia),
        ),
      ],
    );
  }
}