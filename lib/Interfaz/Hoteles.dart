import 'package:appproyect/Constants.dart';
import 'package:flutter/material.dart';
import 'package:appproyect/Interfaz/Lugar.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class Hotel extends Lugares{
  double NoEstrellas;
  int NoHabitaciones;

  @override
  Widget Mostrar({@required String lugar,@required String descripcion,@required String direccion,@required int noHabitaciones,@required double noEstrellas}) {
    // TODO: implement Mostrar
    return Column(
      children: <Widget>[
        Container(child: Text(lugar,style: kTextLugar,),padding: EdgeInsets.all(5),),
        Container(
          child: Text(direccion,style: kTextUbicacion,),padding: EdgeInsets.all(5),
        ),
        Row(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Container(
                padding: EdgeInsets.only(top:10,bottom: 10),
                margin: EdgeInsets.only(left: 15,right: 10),
                child: Text(descripcion,textAlign: TextAlign.center,style: kTextResenia),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                padding: EdgeInsets.only(top:10,bottom: 10),
                margin: EdgeInsets.only(left: 15,right: 10),
                child: Column(
                  children: <Widget>[
                    Text('ROOMS',style: kTextResenia,),
                    Text(noHabitaciones.toString(),style: kTextResenia,),
                    SizedBox(height: 60,),
                    SmoothStarRating(rating: noEstrellas,color: Colors.amberAccent,borderColor: Colors.amber,),
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
