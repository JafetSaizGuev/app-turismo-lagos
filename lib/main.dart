import 'package:appproyect/Data/Data.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'views/MapView.dart';
import 'views/DetailsView.dart';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'Constants.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Magic Map Turistic',
      theme: ThemeData.dark(),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Home(),
        ),
      ),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  
  informacion info = new informacion();
  Idioma _control;
  int _index;

  double _screenHeight;
  double _bottomSize = 160;
  double _defaultBottomSize = 160;

  BitmapDescriptor _iconMarkerH,
      _iconMarkerR,
      _iconMarkerI,
      _iconMarkerB,
      _iconMarkerP;

  void _createMarkerR() async {
    _iconMarkerR = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(), 'images/restaurant.png');
  }

  void _createMarkerH() async {
    _iconMarkerH = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(), 'images/hotel.png');
  }

  void _createMarkerI() async {
    _iconMarkerI = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(), 'images/map-point.png');
  }

  void _createMarkerP() async {
    _iconMarkerP = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(), 'images/hotel1.png');
  }

  void _createMarkerB() async {
    _iconMarkerB = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(), 'images/Puente.png');
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _createMarkerR();
    _createMarkerI();
    _createMarkerH();
    _createMarkerP();
    _createMarkerB();
    _index = 0;
  }

  @override
  Widget build(BuildContext context) {
    _screenHeight = MediaQuery.of(context).size.height;
    return Stack(
      children: <Widget>[
        VistaMapa(
          Refugio: (){setState(() {  _index = 0;  });},
          Luz: (){setState(() {  _index = 1;  });},
          Puente: (){setState(() {  _index = 2;  });},
          Merced: (){setState(() {  _index = 3;  });},
          Asuncion: (){setState(() {  _index = 4;  });},
          Teatro: (){setState(() {  _index = 5;  });},
          Santuario: (){setState(() {  _index = 6;  });},
          Plaza: (){setState(() {  _index = 7;  });},
          Cultura: (){setState(() {  _index = 8;  });},
          Precidencia: (){setState(() {  _index = 9;  });},
          Moscati: (){setState(() {  _index = 10;  });},
          Calvario: (){setState(() {  _index = 11;  });},
          Moya: (){setState(() {  _index = 12;  });},
          Colonial: (){setState(() {  _index = 13;  });},
          Inn: (){setState(() {  _index = 14;  });},
          Paris: (){setState(() {  _index = 15;  });},
          Troje: (){setState(() {  _index = 16;  });},
          Victoria: (){setState(() {  _index = 17;  });},
          CGrande: (){setState(() {  _index = 18;  });},
          Galerias: (){setState(() {  _index = 19;  });},
          Black: (){setState(() {  _index = 20;  });},
          Zul: (){setState(() {  _index = 21;  });},
          Tete: (){setState(() {  _index = 22;  });},
          Sorato: (){setState(() {  _index = 23;  });},
          Salads: (){setState(() {  _index = 24;  });},
          Remedio: (){setState(() {  _index = 25;  });},
          Anden: (){setState(() {  _index = 26;  });},
          Wini: (){setState(() {  _index = 27;  });},
          Lumaro: (){setState(() {  _index = 28;  });},
          Mancha: (){setState(() {  _index = 29;  });},
          Ochavada: (){setState(() {  _index = 30;  });},
          Bolonia: (){setState(() {  _index = 31;  });},
          Bre: (){setState(() {  _index = 32;  });},
          Italian: (){setState(() {  _index = 33;  });},
          CalleJuela: (){setState(() {  _index = 34;  });},
          Dilan: (){setState(() {  _index = 35;  });},
          Francos: (){setState(() {  _index = 36;  });},
          Rincon: (){setState(() {  _index = 37;  });},
          Vina: (){setState(() {  _index = 38;  });},
          Saroman: (){setState(() {  _index = 39;  });},
          Terrascali: (){setState(() {  _index = 40;  });},
          Bife: (){setState(() {  _index = 41;  });},
          Rinconada: (){setState(() {  _index = 42;  });},
          Rivera: (){setState(() {  _index = 43;  });},
          Carol: (){setState(() {  _index = 44;  });},
          Chava: (){setState(() {  _index = 45;  });},
          Mariano: (){setState(() {  _index = 46;  });},
          Meson: (){setState(() {  _index = 47;  });},
          iconB: _iconMarkerB,
          iconH: _iconMarkerH,
          iconI: _iconMarkerI,
          iconP: _iconMarkerP,
          iconR: _iconMarkerR,
        ),
        Positioned(
          bottom: 0,
            child: Container(
              child: GestureDetector(
                onVerticalDragUpdate: (val) {
                setState(() => _bottomSize = _screenHeight - val.globalPosition.dy);
              },
                child: AnimatedContainer(
                  duration: Duration(milliseconds: 100),
                  width: MediaQuery.of(context).size.width,
                  height: _bottomSize,
                  constraints: BoxConstraints(
                  minHeight: _defaultBottomSize,
                  maxHeight: _screenHeight * 0.40,
                  ),
                decoration: BoxDecoration(
                  color: Colors.black87,
                  borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25),
                  topRight: Radius.circular(25),
                  ),
                ),
                  child: Column(
                    children: <Widget>[
                    Container(
                      width: 50,
                      height: 5,
                      margin: EdgeInsets.symmetric(vertical: 10),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                      color: Colors.blueGrey[200],
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                    ),
                  ),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                        children: <Widget>[
                          VistaDetalles(ElDetalle: info.RegresarInfo(_control, _index),),
                        ],
                      ),
                    ),
                  ),],
                ),
              ),
            ),
          ),
        ),
        CambiarIdioma(
          AccionESP: (){setState(() {
            _control = Idioma.Esp;
            Fluttertoast.showToast(msg: 'Español',backgroundColor: ktoast);
          });},
          AccionING: (){setState((){
            _control = Idioma.Ing;
            Fluttertoast.showToast(msg: 'English',backgroundColor: ktoast);
          });},
          AccionJAP: (){setState((){
            _control = Idioma.Jap;
            Fluttertoast.showToast(msg: '日本語',backgroundColor: ktoast);
          });},
        ),
      ],
    );
  }
}

class CambiarIdioma extends StatelessWidget {
  CambiarIdioma({this.AccionESP,this.AccionING,this.AccionJAP});
  final Function AccionESP;
  final Function AccionING;
  final Function AccionJAP;
  @override
  Widget build(BuildContext context) {
    return SpeedDial(
      animatedIcon: AnimatedIcons.menu_close,
      overlayOpacity: 0.0,
      marginBottom: 30,
      marginRight: 30,
      children: [
        SpeedDialChild(
          child: Image.asset('images/espaniol.png'),
          onTap: AccionESP,
        ),
        SpeedDialChild(
          child: Image.asset('images/ingles.png'),
          onTap: AccionING,
        ),
        SpeedDialChild(
          child: Image.asset('images/japones.png'),
          onTap: AccionJAP,
        ),
      ],
    );
  }
}

