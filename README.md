# app proyect turismo

App Movil de turismo en la ciudad de Lagos de Moreno, Jal. en múltiples idiomas.
Usa la localización del dispositivo para cuadrar su posición en la ciudad.
Muestra los distintos puntos de interés en el centro histórico de la ciudad como hoteles, restaurantes o construcciones antiguas.

<p align="center">
	<img alt="App final" src="https://www.dropbox.com/s/okyqnz36yjay2z6/P01.gif" height="200px">
</p>

## Getting Started

<p align="center">
	<img alt="permisos" src="https://www.dropbox.com/s/56d2zrmqdfltf3n/Permisos.png" height="200px">
	<img alt="Funcionamiento" src="https://www.dropbox.com/s/qrf47llyehskpj2/I02.gif" height="200px">
</p>
